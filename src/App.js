import './App.css';
import MyCard from './MyCard';
import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Form,Button } from 'react-bootstrap';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
        currentTitle : '',
        card : []
    }
    
  }
  AddNewCard(e){
      this.state.card.push({key : Math.random(),title:this.state.currentTitle})
      this.setState({
        currentTitle : '',
      })
      e.preventDefault();
  }
  GetText(e){
    this.setState({currentTitle:e.target.value})
  }
  RemoveCard(key){
      let updateCard = this.state.card;
      updateCard.splice(updateCard.findIndex(data => data.key === key),1);
      this.setState  ({
        currentTitle : '',
        card : updateCard
      });
  }
  render() {
    return (
      
      <div className="container">
          <Form onSubmit = {(e)=>{this.AddNewCard(e)}}>
            <Form.Group controlId="formBasicPassword">
              <Form.Label>Input title</Form.Label>
              <Form.Control type="text" id placeholder="Input title" onChange = {(e)=>{this.GetText(e)}} value={this.state.currentTitle}/>
            </Form.Group>
            <Button variant="info" type="submit" >
              Submit
            </Button>
          </Form>
      <div className="container">
        <div className="row">
            {this.state.card.map((data)=>
              <MyCard onRemove = {()=>this.RemoveCard(data.key)} key={data.key} title={data.title}></MyCard>
            )}
        </div>
      </div>
    </div>
    )
  }
}

