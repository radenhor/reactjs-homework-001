import React, { Component  } from 'react'
import './MyStyle.css';
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card } from 'react-bootstrap';


export default function MyCard(props) {
    return (
        <div className="col-md-4 p-0" onClick={()=>{props.onRemove(props.key)}}>
            <Card className="mt-1 mr-2" bg="info" text="white" style={{ width: '18rem' }}>
            <Card.Header>{props.title}</Card.Header>
            <Card.Body>
            <Card.Title><center><h1 className="text-dark">{props.title[0]}</h1></center></Card.Title>
            </Card.Body>
            </Card>
        </div>
    )
}
